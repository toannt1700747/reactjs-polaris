import React from 'react';
import en from '@shopify/polaris/locales/en.json';
import { AppProvider } from '@shopify/polaris';
import AppLayout from './layouts/AppLayout';
function App() {
  return (
    <AppProvider
      i18n={en}
      theme={{
        logo: {
          width: 105,
          topBarSource: 'https://cdn1.avada.io/logo/avada_logo_final_color.png',
        },
      }}
    >
      <AppLayout />
    </AppProvider>
  );
}

export default App;
