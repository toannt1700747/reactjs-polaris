import { Form, Modal, TextField } from '@shopify/polaris';
import React, { useCallback, useEffect, useState } from 'react';

const TodoModal = ({
  active,
  handleChange = () => {},
  handleSubmit = () => {},
  loading,
}) => {
  const [value, setValue] = useState('');
  const handleInput = useCallback((value) => {
    setValue(value);
  }, []);

  useEffect(() => {
    if (!active) {
      setValue('');
    }
  }, [active]);

  return (
    <Modal
      open={active}
      onClose={handleChange}
      title="Create a new Todo"
      primaryAction={{
        content: 'Create',
        onAction: () => {
          handleSubmit(value);
        },
        disabled: !value,
        loading: loading,
      }}
      secondaryActions={[
        {
          content: 'Cancle',
          onAction: handleChange,
        },
      ]}
    >
      <Modal.Section>
        <Form
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit(value);
          }}
        >
          <TextField
            value={value}
            onChange={handleInput}
            placeholder="This is my todo name"
            autoComplete="off"
          />
        </Form>
      </Modal.Section>
    </Modal>
  );
};

export default TodoModal;
