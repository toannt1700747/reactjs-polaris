import { useState, useEffect } from 'react';
import axiosTodo from '../helpers/api/axiosTodo';

const useFetchTodo = ({ url }) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  async function fetchApi() {
    try {
      setLoading(true);
      const { data } = await axiosTodo.get(url);
      setData(data.data);
    } catch (e) {
      console.error(e);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchApi();
  }, []);

  return {
    data,
    setData,
    loading,
    setLoading,
  };
};

export default useFetchTodo;
