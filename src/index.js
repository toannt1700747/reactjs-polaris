import React from 'react';
import { render } from 'react-dom';
import App from './App';
import '@shopify/polaris/dist/styles.css';

render(<App />, document.getElementById('root'));
