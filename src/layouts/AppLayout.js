import { Frame, TopBar } from '@shopify/polaris';
import TodoList from '../pages/Todo/TodoList';
import React from 'react';

const AppLayout = () => {
  const userMenuMarkup = <TopBar.UserMenu name="Avada Group" initials="A" />;

  const topBarMarkup = <TopBar userMenu={userMenuMarkup} />;
  return (
    <Frame topBar={topBarMarkup}>
      <TodoList />
    </Frame>
  );
};

export default AppLayout;
