import {
  Page,
  Card,
  ResourceList,
  ResourceItem,
  TextStyle,
  Button,
  Layout,
  Badge,
  Stack,
} from '@shopify/polaris';
import React, { useCallback, useState } from 'react';
import TodoModal from '../../components/TodoModal/TodoModal';
import useFetchTodo from '../../hooks/useFetchTodo';
import axiosTodo from '../../helpers/api/axiosTodo';

const TodoList = () => {
  const [selectedItems, setSelectedItems] = useState([]);
  const [loadingModal, setLoadingModal] = useState(false);
  const {
    data: todos,
    setData: setTodos,
    loading,
    setLoading,
  } = useFetchTodo({ url: 'todos' });
  const [active, setActive] = useState(false);

  const resourceName = {
    singular: 'todo',
    plural: 'todos',
  };

  const promotedBulkActions = [
    {
      content: 'Complete',
      onAction: () => completeTodo(selectedItems),
    },
    {
      content: 'Delete',
      onAction: () => removeTodo(selectedItems),
    },
  ];

  const handleModal = useCallback(() => setActive(!active), [active]);

  const addTodo = async (title) => {
    try {
      setLoadingModal(true);
      const { data } = await axiosTodo.post('/todos', {
        title,
      });
      if (data.success) {
        setTodos((prevTodo) => [data.data, ...prevTodo]);
      }
    } catch (e) {
      console.error(e);
    } finally {
      setLoadingModal(false);
      setActive(false);
    }
  };

  const completeTodo = async (arr) => {
    try {
      setLoading(true);
      const { data } = await axiosTodo.put('/todos', arr);

      if (data.success) {
        const updateTodos = todos.map((todo) => {
          if (arr.includes(todo.id)) {
            return {
              ...todo,
              complete: true,
            };
          }
          return todo;
        });
        setTodos(updateTodos);
      }
    } catch (e) {
      console.error(e);
    } finally {
      setLoading(false);
      setSelectedItems([]);
    }
  };

  const removeTodo = async (arr) => {
    try {
      setLoading(true);
      const { data } = await axiosTodo.delete('/todos', {
        data: arr,
      });
      if (data.success) {
        const updateTodos = todos.filter((todo) => !arr.includes(todo.id));
        setTodos(updateTodos);
      }
    } catch (e) {
      console.error(e);
    } finally {
      setLoading(false);
      setSelectedItems([]);
    }
  };

  return (
    <Page
      title="Todos"
      primaryAction={{ content: 'Create', onAction: () => handleModal() }}
    >
      <Layout sectioned>
        <Card>
          <ResourceList
            resourceName={resourceName}
            items={todos}
            renderItem={renderItem}
            selectedItems={selectedItems}
            onSelectionChange={setSelectedItems}
            promotedBulkActions={promotedBulkActions}
            loading={loading}
          />
        </Card>
        <TodoModal
          active={active}
          handleChange={handleModal}
          handleSubmit={addTodo}
          loading={loadingModal}
        />
      </Layout>
    </Page>
  );

  function renderItem(item) {
    const { id, title, complete } = item;
    return (
      <ResourceItem id={id} verticalAlignment="center">
        <Stack alignment="center" distribution="equalSpacing">
          <TextStyle variant="bodyMd" as="label">
            {title}
          </TextStyle>
          <Stack alignment="center">
            <Badge status={complete ? 'success' : ''}>
              {complete ? 'done' : 'pending'}
            </Badge>
            <Button onClick={() => completeTodo([id])}>Complete</Button>
            <Button destructive onClick={() => removeTodo([id])}>
              Delete
            </Button>
          </Stack>
        </Stack>
      </ResourceItem>
    );
  }
};

export default TodoList;
